(setq-default indent-tabs-mode nil);

(add-to-list 'load-path "~/.emacs.d/lisp/")

(require 'project-explorer)
(require 'flymake-jshint)

(setq jshint-configuration-path nil)
(setq flymake-log-level 0)
(add-hook 'js-mode-hook 'flymake-jshint-load)
(custom-set-variables
 '(help-at-pt-timer-delay 0.5)
 '(help-at-pt-display-when-idle '(flymake-overlay)))
